class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(players)
    @players = players # contains options hash of referee and guesser
    @guesser = @players[:guesser]
    @referee = @players[:referee]
  end

  def setup
    secret_length = @referee.pick_secret_word
    @guesser.register_secret_length(secret_length)
    @board = Array.new(secret_length)
  end

  def take_turn
    user_guess = @guesser.guess
    @referee.check_guess(user_guess)
    self.update_board
    @guesser.handle_response
  end

  def update_board
  end
end

class HumanPlayer
end

class ComputerPlayer
  def initialize(dictionary)
    @d = dictionary
  end

  def pick_secret_word
    @d.join.length
  end

  def check_guess(letter)
    arr = []
    @d.join.each_char.with_index { |ch, idx| letter.include?(ch) ? arr << idx : next }
    arr
  end

  def register_secret_length(secret_length)
    @d = @d.select { |word| word.length == secret_length }
  end

  def guess(board)
    ch_count = Hash.new(0)
    @d.join.each_char { |ch2| ch_count[ch2] += 1 }
    board.each do |ch|
      ch_count.delete(ch)
    end

    ch_count.sort_by { |_, v| v } [-1].first
  end

  def candidate_words
    @d
  end

  def handle_response(ch, arr)
    @d = @d.select! do |word|
      (0...word.length).select { |idx| word[idx] == ch } == arr
    end

    @d
  end
end
